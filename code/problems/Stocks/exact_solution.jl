include(joinpath(@__DIR__,"../../lib/Wiener process/main.jl"))

using JuMP
using Cbc
using LinearAlgebra

m = Model(Cbc.Optimizer)

index_a = 1:n

c_0 = Matrix{Float64}(undef, 2, n)
c_01 = Matrix{Float64}(undef, 1, n)
c_02 = Matrix{Float64}(undef, 1, n)

for i=1:n
    c_01[i] = - W[i]
    c_02[i] = 1
end

c_0 = [c_01; c_02]
#println(c_0)
c = [1 W[n+1]] * c_0

#println(c)

@variable(m, 1>= a[index_a] >=-1, Int)

@objective(m, Max, sum( c[i]*a[i] for i=1:n) )

A = Matrix{Float64}(undef, 2*n, n)
fill!(A, zero(Float64))

for k=1:n
    for i=1:k
        A[2*k-1,i] = - W[i]
        A[2*k, i] = 1
    end
end

b = Matrix{Float64}(undef, 1, 2*n) #d_0 = 1000; s_0 = 0
for i=1:(2*n)
    if i%2==1
        b[i] = -1000
    else
        b[i] = 0
    end
end
#println(A)
#println(b)


@constraint(m, constraint[j=1:(2*n)], sum( A[j,i]*a[i] for i=1:n ) >= b[j] )
#
# @constraint(m, bound, x[1] <= 10)

print(m) #printing the prepared optimization model

optimize!(m)

println("Optimal Solutions:")

s = 0.0

for i=1:n
    global s= s + value(a[i])
    println("a[$i] = ", value(a[i]), ";  s[$i] = ", s)
end

println("Total capital: ", (objective_value(m) + 1000)*(1-1e-4)^n)
