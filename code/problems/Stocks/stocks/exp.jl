using Dates #modules utilisés
using Random

println("Debug 1 ", Dates.now())
include("environment.jl")
println("Debug 2 ", Dates.now())

using .WienerEnv
println("Debug 3 ", Dates.now())

include("exact_solution.jl")
using .Exact_sol
println("Debug 4 ", Dates.now())

using ReinforcementLearning
using Random
using Flux
using Logging
using TensorBoardLogger
using BSON: @save, @load

println("Debug 5 ", Dates.now())

@static if Sys.iswindows()
    using Plots
else
    using PyPlot
end

inner_env = WienerEnvironment()

rng = MersenneTwister(123) #random number generator

init = glorot_uniform(rng)

ns, na = length(get_state(inner_env)), length(get_actions(inner_env)) #nombre d'états et d'actions

N_FRAMES = 4

env =
    inner_env |>
    StateOverriddenEnv(StackFrames(ns, N_FRAMES), ) |>
    StateCachedEnv

agent = Agent(
    policy = QBasedPolicy( #policy de Q learning
        learner = BasicDQNLearner( #apprentissage de Q learning
            approximator = NeuralNetworkApproximator(
                model = Chain(
                    Reshaper(ns * N_FRAMES),                                        #, size(x, ndims(x))),        # quatre layers de neurones : frames*nombre d'états,
                    Dense(ns * N_FRAMES, 32, relu; initW = init),                   # 32, 32, nombre d'actions
                    Dense(32, 32, relu; initW = init),
                    Dense(32, na, relu; initW = init),
                ) |> cpu,
                optimizer = ADAM(),
            ),
            batch_size = 16,
            min_replay_history = 10,
            loss_func = huber_loss,
            rng = rng,
        ),
        explorer = EpsilonGreedyExplorer(       # facteur d'exploration : décroissance exponentielle
            kind = :exp,
            ϵ_stable = 0.2,
            decay_steps = 100,
            rng = rng,
        ),
    ),
    trajectory = CircularCompactSARTSATrajectory(;
        capacity = 100,
        state_type = Float32,
        state_size = (ns, N_FRAMES)
    ),
)

t = Dates.format(now(), "yyyy_mm_dd_HH_MM_SS")
save_dir = joinpath(pwd(), "checkpoints", "WienerExperiment_$(t)")      #emplacement de sauvegarde et nom

lg = TBLogger(joinpath(save_dir, "tb_log"), min_level = Logging.Info)

total_reward_per_episode = TotalRewardPerEpisode()
time_per_step = TimePerStep()
steps_per_episode = StepsPerEpisode()

update_freq = 4

hook = ComposedHook(
    total_reward_per_episode,
    time_per_step,
    steps_per_episode,
    DoEveryNStep(update_freq) do t, agent, env
        with_logger(lg) do
            @info "training" loss = agent.policy.learner.loss log_step_increment = update_freq
        end
    end,
    DoEveryNEpisode() do t, agent, env
        with_logger(lg) do
            @info "training" episode_length = steps_per_episode.steps[end] reward = total_reward_per_episode.rewards[end] log_step_increment = 0
        end
    end,
)

# eval_hook = DoEveryNStep(1)  do t, agent, env
#     println("Inner:", t)
# end
# stop_condition = StopAfterStep(9)

N_TRAINING_STEPS = 1000       #entrainement de l'agent avant le test réel

run(
    agent,
    env,
    StopAfterStep(N_TRAINING_STEPS), # début de l'entrainement
    DoEveryNStep(1) do t, agent, env
        eval_stop_condition = StopAfterStep(42, cur = 1, is_show_progress = true)
        Flux.testmode!(agent)
        run(agent, env, eval_stop_condition, hook)
        Flux.trainmode!(agent)
    end
    )

println("Saving model")
@save "stocks_model.bson" agent

Flux.testmode!(agent)
RLBase.reset!(env)
run(agent, env, StopAfterStep(42, cur=1, is_show_progress=true), hook)

println("Agent: ")
println(agent)

println("Capital ", get_capital(inner_env))
println("Dollars ", inner_env.dollars)
println("Stocks ", inner_env.stocks)
W = inner_env.wiener_process;

_, optimal_capital = FindExactSolution(length(W) - 1 , W)
println("Capital in optimal solution: ", optimal_capital)

@static if Sys.iswindows()
    p = plot(1:length(W), W)
    display(p)
else
    plot(1:length(W), W)
    show()
end
