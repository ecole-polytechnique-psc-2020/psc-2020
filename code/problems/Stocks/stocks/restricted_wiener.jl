module WienerEnv

using Random
using RandomNumbers

export generate_restricted_wiener_process
export generate_restricted_geometric_brownian_motion_process
export Reshaper

struct Reshaper
    fst_dim::Int64
end

(reshaper::Reshaper)(x) = reshape(x, reshaper.fst_dim, :)


"Generate values of Wiener process in specified points (without zero, it's added automatically; MUST be positive and strictly ascending) with condition `W[points[end]]== value_in_last_point`"
function generate_restricted_wiener_process(points::Array{Float64, 1}, value_in_last_point::Float64; rng=Random.GLOBAL_RNG)::Array{Float64, 1}
    if length(points) == 0
        return [0.0]
    end

    increments = randn(rng, length(points))

    process = [0.0]
    previous_point = 0.0

    for i in 1:length(points)
        push!(process, process[end] + increments[i] * sqrt(points[i] - previous_point))
        previous_point = points[i]
    end

    delta = (value_in_last_point - process[end]) / points[end]
    for i in 1:length(points)
        process[i + 1] += points[i] * delta
    end

    return process
end

function generate_restricted_geometric_brownian_motion_process(points::Array{Float64, 1}, mu::Float64, sigma::Float64, S0::Float64, value_in_last_point::Float64; rng=Random.GLOBAL_RNG)::Array{Float64, 1}
    if length(points) == 0
        return [S0]
    end

    v = mu - sigma ^ 2 / 2
    wiener_in_last_point = (log(value_in_last_point / S0) - v * points[end]) / sigma
    process = generate_restricted_wiener_process(points, wiener_in_last_point)

    process[1] = S0
    for i in 2:length(process)
        process[i] = S0 * exp(v * points[i - 1] + sigma * process[i])
    end

    return process
end

end # module
