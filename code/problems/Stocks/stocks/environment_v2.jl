__precompile__()

module WienerEnv

using ReinforcementLearningBase
using DifferentialEquations
using RandomNumbers

export WienerEnvironmentV2, get_capital, action, Reshaper

struct Reshaper
    fst_dim::Int64
end

(reshaper::Reshaper)(x) = reshape(x, reshaper.fst_dim, :)

mutable struct WienerEnvironmentV2 <: AbstractEnv
    dollars::Float64
    stocks::Float64
    stock_price::Float64
    old_stock_price::Float64
    old_capital::Float64

    seed::UInt64
    preserve_seed::Bool

    wiener_process::NoiseProcess
    gamma::Float64
    days_without_reward::Int64
    current_day::Int64
end

function WienerEnvironmentV2(seed)
    env = WienerEnvironmentV2(1000, 0, 1, 1, 1000, seed, true, GeometricBrownianMotionProcess(1.0, 0.7, 0.0, 1.0; rng=Xorshifts.Xoroshiro128Plus(seed)), 1.0, 0, 0)
    env.wiener_process.dt = 0.1
    setup_next_step!(env.wiener_process, nothing, nothing)
    env
end

function WienerEnvironmentV2()
    env = WienerEnvironmentV2(rand(UInt64))
    env.preserve_seed = false
    env
end

RLBase.get_actions(env::WienerEnvironmentV2) = Vector{Int64}(1:101)

get_capital(env::WienerEnvironmentV2)::Float64 = env.dollars + env.stock_price * env.stocks

# RLBase.get_reward(env::WienerEnvironment) = (1 - env.inflation_coefficient) * get_capital(env) - env.old_capital
function RLBase.get_reward(env::WienerEnvironmentV2)
    if env.current_day >= env.days_without_reward
        result = get_capital(env) ^ env.gamma - env.old_capital ^ env.gamma
    else
        result = 0
    end
    println("get_reward() -> ", result)
    result
end

function RLBase.get_state(env::WienerEnvironmentV2)
    result = zeros(4)

    result[1] = log(get_capital(env))
    result[2] = get_reward(env)
    result[3] = env.stock_price / env.old_stock_price
    result[4] = env.stock_price * env.stocks / get_capital(env)

    result
end

RLBase.get_terminal(env::WienerEnvironmentV2) = get_capital(env) <= 0

function RLBase.reset!(env::WienerEnvironmentV2)
    println("RLBase.reset!(WienerEnvironmentV2)")
    env.dollars = 1000
    env.stocks = 0
    env.stock_price = 1
    env.old_stock_price = 1
    env.old_capital = 1000

    if !env.preserve_seed
        env.seed = rand(UInt64)
    end

    env.current_day = 0

    env.wiener_process = GeometricBrownianMotionProcess(1.0, 0.7, 0.0, 1.0; rng=Xorshifts.Xoroshiro128Plus(env.seed))
    env.wiener_process.dt = 0.1
    setup_next_step!(env.wiener_process, nothing, nothing)
end

function (env::WienerEnvironmentV2)(action)
    real_action = (action - 1) / 100.0
    print("action: ", action, " (in fact, ", real_action, "); ")

    if env.current_day >= env.days_without_reward
        env.old_capital = get_capital(env)
    end

    env.old_stock_price = env.stock_price

    c = get_capital(env)
    old_stocks = env.stocks
    env.stocks = round(real_action * c / env.stock_price)
#    env.stocks = real_action * c / env.stock_price
    env.dollars = c - env.stocks * env.stock_price

    println("delta_stocks: ", env.stocks - old_stocks)

    accept_step!(env.wiener_process, 0.1, nothing, nothing)
    env.stock_price = env.wiener_process[end]

    env.current_day += 1
end

end # module
