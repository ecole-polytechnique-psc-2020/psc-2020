#corps du programme pour une policy aléatoire


using Dates

println("Debug 1", Dates.now())
include("environment.jl")       #importation de l'environnement
println("Debug 3", Dates.now())

using .WienerEnv
println("Debug 2.5", Dates.now())

using ReinforcementLearningBase
using ReinforcementLearningCore
using PyPlot

println("Debug 2", Dates.now())

env = WienerEnvironment()       #construction d'un nouvel environnement
hook = DoEveryNStep(1)  do t, agent, env println(t) end     #description de l'action à effectuer entre chaque episode de l'algorithme
run(Agent(;policy=RandomPolicy(env)), env, StopAfterStep(101), hook)        #execution de l'algorithme sur l'environnement env, avec une policy aléatoire,s'arrêtant au bout de 101 étapes

#affichage des résultats (chiffres et courbes)
println("Capital ", get_capital(env))
println("Dollars ",env.dollars)
println("Stocks ",env.stocks)
W = env.wiener_process;
plot(1:length(W), W)
show()
