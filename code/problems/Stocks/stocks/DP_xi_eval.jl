include("restricted_wiener.jl")
include("environment_predefined.jl")
using .WienerEnv
using .WienerEnvPredefined

using Statistics
using BSON: @load
using PyPlot
using Printf
println("Debug qqq")

@load "dp_xi.bson" A V

dNSP = 100
maxPriceInit=1.0

dt=0.1
mu=1.0
sig=0.7
Q95=1.645
Q90=1.282

discreteNstepStockPrice=100
dNSP=discreteNstepStockPrice
maxPriceInit=1.0
maxPriceFactor = exp((mu-sig*sig/2)dt+sig*Q95*sqrt(dt))

s = 30
samples = 100

if length(ARGS) >= 1
    s = parse(Int, ARGS[1])
end

println("Days: ", s)

# maxPrice=maxPriceInit*(maxPriceFactor^s)
maxCapitalInit = maxPriceInit * 1000.0

function maxCapital(day)
    maxCapitalInit * maxPriceFactor ^ day
end

function maxPrice(day)
    maxPriceInit * maxPriceFactor ^ day
end


x = zeros(100)
y = zeros(100)
z = zeros(100)

for NPriceTarget in 1:100
    x[NPriceTarget]=NPriceTarget*maxPrice(s)/dNSP

    ySamples = zeros(samples)
    zSamples = zeros(samples)
    for i in 1:samples
        println("i: ", i)
        process = generate_restricted_geometric_brownian_motion_process(Array(0.1:0.1:s*0.1), 1.0, 0.7, 1.0, x[NPriceTarget])
        env = WienerEnvironmentPredefined(process)

        for day in 1:s
            println("day: ", day)
            capital = get_capital(env); println("capital: ", capital)
            stock = env.stock_price; println("stock: ", stock)
            xi = env.stock_price * env.stocks / capital; println("xi: ", xi)
            NCapital = max(0, min(100, Int(round(capital / maxCapital(day) * 100))))
            NPrice = max(1, min(100, Int(round(stock / maxPrice(day) * 100))))
            action = A[s+1, 1 + NCapital, min(1 + Int(round(xi * 100)), 101), NPrice] + 1
            env(action)
        end

        ySamples[i] = (env.last_action - 1) / 100.0
        zSamples[i] = get_capital(env)
    end

    y[NPriceTarget] = mean(ySamples)
    z[NPriceTarget] = mean(zSamples)
end

figure(;figsize=(8,12))
subplot(211)
title("DP-based results in day $s")
ylim(-0.05, 1.05)
plot(x,y)
xlabel("Stock price")
ylabel("Action")

subplot(212)
title("DP-based capitals in day $s")
plot(x, z)
xlabel("Stock price")
ylabel("Capital")
# show()
savefig(Printf.@sprintf "output/DP_xi_eval_%d.png" s)

open((Printf.@sprintf "output/DP_xi_eval_%d.txt" s), "w") do f
    println(f, "priceGrid\tactions\tcapitals")
    for i in 1:100
        println(f, x[i], '\t', y[i], '\t', z[i])
    end
end
