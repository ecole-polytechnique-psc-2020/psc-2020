#!/bin/bash

for i in $(seq 1 40); do
    julia -J../bin/sys.so ./plot_rl_response.jl $i
    julia -J../bin/sys.so ./DP_xi_eval.jl $i
done
