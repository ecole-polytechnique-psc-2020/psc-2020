using Dates #modules utilisés
using Random

println("Debug 1 ", Dates.now())
include("environment.jl")
println("Debug 2 ", Dates.now())

using .WienerEnv
println("Debug 3 ", Dates.now())

include("exact_solution.jl")
using .Exact_sol
println("Debug 4 ", Dates.now())

using ReinforcementLearning
using Flux
using BSON: @save, @load
using ArgParse
using PyPlot
using Printf
using MultivariateStats

println("Debug 5 ", Dates.now())


function extract_arguments()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--seed"
            help = "seed for random generator (optional)"
            arg_type = UInt64
        "--model"
            help = "Model file (e.g. stocks_model.bson)"
            arg_type = String
            required = true
        "--days"
            help = "session length in days"
            arg_type = Int
            range_tester = x -> x > 0
            default = 42
        "oneshot"
            help = "Oneshot mode"
            action = :command
        "batch"
            help = "Batch mode"
            action = :command
    end
    @add_arg_table s["batch"] begin
        "--size"
            help = "Batch size"
            arg_type = Int
            range_tester = x -> x > 0
            default = 20
    end

    return parse_args(s)
end

function update_seed(args)
    if args["seed"] == nothing
        args["seed"] = rand(UInt64)
    end

    println("Using seed: ", args["seed"])
    Random.seed!(args["seed"])
end

function load_agent(args)
    agent = BSON.load(args["model"])[:agent]
    Flux.testmode!(agent)
    agent
end

function build_env(inner_env)
    ns, na = length(get_state(inner_env)), length(get_actions(inner_env)) #nombre d'états et d'actions

    N_FRAMES = 4

    env =
        inner_env |>
        StateOverriddenEnv(StackFrames(ns, N_FRAMES), ) |>
        StateCachedEnv

    env
end

function simulate_agent(inner_env, agent, days)
    env = build_env(inner_env)
    trajectory = Vector{Tuple{Float64, Float64, Float64}}()

    push!(trajectory, (inner_env.dollars, inner_env.stocks, inner_env.stock_price))
    run(agent, env, StopAfterStep(days, cur=1),
        DoEveryNStep(1) do t, agent, env
            push!(trajectory, (inner_env.dollars, inner_env.stocks, inner_env.stock_price))
        end
    )

    trajectory
end

function simulate_optimal(inner_env)
    W = inner_env.wiener_process
    solution, _ = FindExactSolution(length(W)-1, W)
    actions = map(x -> Int(round(x)) + 2, solution)

    trajectory = Vector{Tuple{Float64, Float64, Float64}}()

    RLBase.reset!(inner_env)
    push!(trajectory, (inner_env.dollars, inner_env.stocks, inner_env.stock_price))
    for action in actions
        inner_env(action)
        push!(trajectory, (inner_env.dollars, inner_env.stocks, inner_env.stock_price))
    end

    trajectory
end

function plot_trajectory(trajectory)
    dollars = map(x -> x[1], trajectory)
    priced_stocks = map(x -> x[2] * x[3], trajectory)

    stackplot(1:length(trajectory), [dollars, priced_stocks]; labels=["Dollars", "Stocks"])
    legend()
    xlabel("Days")
    ylabel("Capital")
end

function trace_trajectory(trajectory)
    for (index, (dollars, stocks, stock_price)) in enumerate(trajectory)
        capital = dollars + stocks * stock_price
        println("Day $(index - 1):\t \$",
                @sprintf("%.2f", dollars), "\t + ", Int(stocks),
                " st.;\tStock price: ", @sprintf("%.2f", stock_price),
                ";\tCapital: ", @sprintf("%.2f", capital))
    end
end

function run_oneshot(args, agent, days)
    inner_env = WienerEnvironment(rand(UInt64))

    agent_trajectory = simulate_agent(inner_env, agent, days)
    optimal_trajectory = simulate_optimal(inner_env)

    println("RL-based trajectory: ")
    trace_trajectory(agent_trajectory)
    println("ILP-based trajectory: ")
    trace_trajectory(optimal_trajectory)

    # Plotting
    W = inner_env.wiener_process

    subplot(311)
    title("Stock price")
    plot(1:length(W), W)
    xlabel("Days")
    ylabel("Price")

    subplot(312)
    title("RL-based trajectory")
    plot_trajectory(agent_trajectory)

    subplot(313)
    title("ILP-based trajectory")
    plot_trajectory(optimal_trajectory)

    show()
end

function run_batch(args, agent, days)
    batch_size = args["size"]
    inner_env = WienerEnvironment(rand(UInt64))

    samples = Vector{Tuple{Float64, Float64}}()

    for i in 1:batch_size
        println("Sample $i/$batch_size")
        agent_last_day = simulate_agent(inner_env, agent, days)[end]
        optimal_last_day = simulate_optimal(inner_env)[end]
        inner_env.seed = rand(UInt64)
        RLBase.reset!(inner_env)

        agent_total = agent_last_day[1] + agent_last_day[2] * agent_last_day[3]
        optimal_total = optimal_last_day[1] + optimal_last_day[2] * optimal_last_day[3]

        push!(samples, (agent_total, optimal_total))
    end

    samples2d = reshape(collect(Iterators.flatten(samples)), 2, :)
    X = samples2d[2,:] # ILP
    Y = samples2d[1,:] # RL

    # Linear Regression: log(Y) = a * log(X) + b
    a, b = llsq(reshape(log.(X), :, 1), log.(Y))
    logXp = LinRange(log(min(X...)), log(max(X...)), 200)
    logYp = a * logXp .+ b

    # Plotting
    title("ILP/RL results comparison")
    scatter(X, Y; label="Samples")
    plot(exp.(logXp), exp.(logYp); label=@sprintf("\$y = %.2fx^{%.2f}\$", exp(b), a), color="red")
    xscale("log")
    yscale("log")
    xlabel("ILP-based capital")
    ylabel("RL-based capital")
    legend()
    show()
end

function main()
    parsed_args = extract_arguments()
    update_seed(parsed_args)
    agent = load_agent(parsed_args)
    days = parsed_args["days"]

    cmd = parsed_args["%COMMAND%"]
    if cmd == "oneshot"
        run_oneshot(parsed_args[cmd], agent, days)
    elseif cmd == "batch"
        run_batch(parsed_args[cmd], agent, days)
    else
        println("Unknown command: $cmd")
        exit(1)
    end
end

main()
