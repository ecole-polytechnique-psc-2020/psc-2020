using Dates #modules utilisés
using Random

println("Debug 1 ", Dates.now())
include("environment_v2.jl")
println("Debug 2 ", Dates.now())

using .WienerEnv
println("Debug 3 ", Dates.now())

using ReinforcementLearning
using Random
using Flux
using Logging
using TensorBoardLogger
using BSON: @save, @load

println("Debug 4 ", Dates.now())

@static if Sys.iswindows()
    using Plots
else
    using PyPlot
end

DAYS = 42
inner_env = WienerEnvironmentV2()
inner_env.days_without_reward = DAYS

rng = MersenneTwister(123) #random number generator

init = glorot_uniform(rng)

ns, na = length(get_state(inner_env)), length(get_actions(inner_env)) #nombre d'états et d'actions

N_FRAMES = 24

env =
    inner_env |>
    StateOverriddenEnv(StackFrames(ns, N_FRAMES), ) |>
    StateCachedEnv

agent = Agent(
    policy = QBasedPolicy( #policy de Q learning
        learner = BasicDQNLearner( #apprentissage de Q learning
            approximator = NeuralNetworkApproximator(
                model = Chain(
                    Reshaper(ns * N_FRAMES),                                        #, size(x, ndims(x))),        # quatre layers de neurones : frames*nombre d'états,
                    Dense(ns * N_FRAMES, 32, relu; initW = init),                   # 32, 32, nombre d'actions
                    Dense(32, 32, relu; initW = init),
                    Dense(32, 64, relu; initW = init),
                    Dense(64, 32, relu; initW = init),
                    Dense(32, na, relu; initW = init),
                ) |> cpu,
                optimizer = ADAM(),
            ),
            batch_size = 16,
            min_replay_history = 10,
            loss_func = huber_loss,
            rng = rng,
        ),
        explorer = EpsilonGreedyExplorer(       # facteur d'exploration : décroissance exponentielle
            kind = :exp,
            ϵ_stable = 0.01,
            decay_steps = 100,
            rng = rng,
        ),
    ),
    trajectory = CircularCompactSARTSATrajectory(;
        capacity = 100,
        state_type = Float32,
        state_size = (ns, N_FRAMES)
    ),
)

t = Dates.format(now(), "yyyy_mm_dd_HH_MM_SS")
save_dir = joinpath(pwd(), "checkpoints", "WienerExperimentV2_$(t)")      #emplacement de sauvegarde et nom

lg = TBLogger(joinpath(save_dir, "tb_log"), min_level = Logging.Info)

total_reward_per_episode = TotalRewardPerEpisode()
time_per_step = TimePerStep()
steps_per_episode = StepsPerEpisode()

update_freq = 4

hook = ComposedHook(
    total_reward_per_episode,
    time_per_step,
    steps_per_episode,
    DoEveryNStep(update_freq) do t, agent, env
        with_logger(lg) do
            @info "training" loss = agent.policy.learner.loss log_step_increment = update_freq
        end
    end,
    DoEveryNEpisode() do t, agent, env
        with_logger(lg) do
            @info "training" episode_length = steps_per_episode.steps[end] reward = total_reward_per_episode.rewards[end] log_step_increment = 0
        end
    end,
)

# eval_hook = DoEveryNStep(1)  do t, agent, env
#     println("Inner:", t)
# end
# stop_condition = StopAfterStep(9)

N_TRAINING_STEPS = 20000       #entrainement de l'agent avant le test réel

run(
    agent,
    env,
    StopAfterStep(N_TRAINING_STEPS), # début de l'entrainement
    DoEveryNStep(1) do t, agent, env
        eval_stop_condition = StopAfterStep(DAYS, cur = 1, is_show_progress = true)
        Flux.testmode!(agent)
        run(agent, env, eval_stop_condition, hook)
        Flux.trainmode!(agent)
    end
    )

println("Saving model")
@save "stocks_N_model_V2.bson" N_FRAMES agent

Flux.testmode!(agent)
RLBase.reset!(env)

capitals = Vector{Float64}([get_capital(inner_env)])
dollars  = Vector{Float64}([inner_env.dollars])
stocks   = Vector{Float64}([inner_env.stocks])

run(agent, env, StopAfterStep(DAYS, cur=1, is_show_progress=true),
    DoEveryNStep(1) do t, agent, env
        push!(capitals, get_capital(inner_env))
        push!(dollars, inner_env.dollars)
        push!(stocks, inner_env.stocks)
    end
   )

println("Agent: ")
println(agent)

println("Capital ", get_capital(inner_env))
println("Dollars ", inner_env.dollars)
println("Stocks  ", inner_env.stocks)
println("Price   ", inner_env.stock_price);
W = inner_env.wiener_process;

@static if Sys.iswindows()
    p = plot(1:length(W), W)
    display(p)
else
    subplot(411)
    plot(W, label="price")
    legend()
    subplot(412)
    plot(capitals, label="capitals")
    legend()
    subplot(413)
    plot(dollars, label="dollars")
    legend()
    subplot(414)
    plot(stocks, label="stocks")
    legend()
    show()
end
