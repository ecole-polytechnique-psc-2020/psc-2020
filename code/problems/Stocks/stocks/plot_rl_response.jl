include("environment_predefined.jl")
include("restricted_wiener.jl")

using .WienerEnv
using .WienerEnvPredefined
using PyPlot
using ReinforcementLearning
using BSON: @load
using Flux
using Random
using Statistics
using Printf

println("Debug qqq")

# @load "../../../../stocks_model_V2.bson" agent
# N_FRAMES = 16
@load "../../../../stocks_N_model_V2.bson" N_FRAMES agent
Flux.testmode!(agent)

function build_env(days, value_in_last_day)
    process = generate_restricted_geometric_brownian_motion_process(Array(0.1:0.1:days*0.1), 1.0, 0.7, 1.0, Float64(value_in_last_day))
    inner_env = WienerEnvironmentPredefined(process)
    ns, na = length(get_state(inner_env)), length(get_actions(inner_env)) #nombre d'états et d'actions
    env =
        inner_env |>
        StateOverriddenEnv(StackFrames(ns, N_FRAMES), ) |>
        StateCachedEnv
    inner_env, env
end

# Some bullshit code
mu = 1.0
sigma = 0.7
dt = 0.1
Q95 = 1.645
maxPriceFactor = exp((mu - sigma^2/2) * dt  + sigma * Q95 * sqrt(dt))
maxPriceInit = 1.0

days = 30
samples = 100

if length(ARGS) >= 1
    days = parse(Int, ARGS[1])
end

println("Days: ", days)

priceGrid = (0:100) * (maxPriceInit * maxPriceFactor ^ days) / 100
actions = []
capitals = []

for price in priceGrid
    possible_actions = zeros(samples)
    possible_capitals = zeros(samples)
    for i in 1:samples
        inner_env, outer_env = build_env(days + 1, price)
        run(agent, outer_env, StopAfterStep(days))
        possible_actions[i] = (inner_env.last_action - 1) / 100.0
        possible_capitals[i] = get_capital(inner_env)
    end
    push!(actions, mean(possible_actions))
    push!(capitals, mean(possible_capitals))
end

figure(;figsize=(8,12))
subplot(211)
title("RL-based results in day $days")
ylim(-0.05, 1.05)
plot(priceGrid, actions)
xlabel("Stock price")
ylabel("Action")

subplot(212)
title("RL-based capitals in day $days")
plot(priceGrid, capitals)
xlabel("Stock price")
ylabel("Capital")
# show()
savefig(Printf.@sprintf "output/plot_rl_response_%d.png" days)

open((Printf.@sprintf "output/plot_rl_response_%d.txt" days), "w") do f
    println(f, "priceGrid\tactions\tcapitals")
    for i in 1:100
        println(f, priceGrid[i], '\t', actions[i], '\t', capitals[i])
    end
end
