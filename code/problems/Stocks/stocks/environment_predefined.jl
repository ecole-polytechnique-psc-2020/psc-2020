__precompile__()

module WienerEnvPredefined

using ReinforcementLearningBase
using DifferentialEquations
using RandomNumbers

export WienerEnvironmentPredefined, get_capital, action, Reshaper

struct Reshaper
    fst_dim::Int64
end

(reshaper::Reshaper)(x) = reshape(x, reshaper.fst_dim, :)

mutable struct WienerEnvironmentPredefined <: AbstractEnv
    dollars::Float64
    stocks::Float64
    stock_price::Float64
    old_stock_price::Float64
    old_capital::Float64

    process::Array{Float64, 1}
    process_index::Int64

    last_action::Int64
end

function WienerEnvironmentPredefined(values::Array{Float64, 1})
    env = WienerEnvironmentPredefined(1000, 0, values[1], 1, 1000, values, 1, 1)
    env
end

RLBase.get_actions(env::WienerEnvironmentPredefined) = Vector{Int64}(1:101)

get_capital(env::WienerEnvironmentPredefined) = env.dollars + env.stock_price * env.stocks

# RLBase.get_reward(env::WienerEnvironment) = (1 - env.inflation_coefficient) * get_capital(env) - env.old_capital
function RLBase.get_reward(env::WienerEnvironmentPredefined)
    log(get_capital(env)) - log(env.old_capital)
end

function RLBase.get_state(env::WienerEnvironmentPredefined)
    result = zeros(4)

    result[1] = log(get_capital(env))
    result[2] = get_reward(env)
    result[3] = env.stock_price / env.old_stock_price
    result[4] = env.stock_price * env.stocks / get_capital(env)

    result
end

RLBase.get_terminal(env::WienerEnvironmentPredefined) = get_capital(env) <= 0 && env.process_index <= length(env.process)

function RLBase.reset!(env::WienerEnvironmentPredefined)
    println("RLBase.reset!(WienerEnvironmentPredefined)")
    env.dollars = 1000
    env.stocks = 0
    env.stock_price = env.process[1]
    env.old_stock_price = 1
    env.old_capital = 1000

    env.process_index = 1
    env.last_action = 1
end

function (env::WienerEnvironmentPredefined)(action)
    env.last_action = action
    real_action = (action - 1) / 100.0
    print("action: ", action, " (in fact, ", real_action, "); ")

    env.old_capital = get_capital(env)
    env.old_stock_price = env.stock_price

    c = get_capital(env)
    old_stocks = env.stocks
    env.stocks = round(real_action * c / env.stock_price)
#    env.stocks = real_action * c / env.stock_price
    env.dollars = c - env.stocks * env.stock_price

    println("delta_stocks: ", env.stocks - old_stocks)

    env.process_index += 1
    env.stock_price = env.process[min(env.process_index, length(env.process))]
end

end # module
