using DifferentialEquations, Plots

n = 40 #number fo episodes

μ = 1.0
σ = 0.7
W = GeometricBrownianMotionProcess(μ,σ,0.0,1.0)


dt = 0.1
W.dt = dt

setup_next_step!(W, nothing, nothing)
for i in 1:n
  accept_step!(W, dt, nothing, nothing)
end

display(Plots.plot(1:(n+1), W, xlabel = "Time", lw = 3, ylabel = "Price", title = "Stocks", label = "Stock1"))
