#!/usr/bin/env JULIA_DEBUG=all julia

using Pkg

Pkg.add("PackageCompiler")

using PackageCompiler

for project in ARGS
    println("Building sysimage for project ", project)
    Pkg.activate(project)

    project_toml_path = Pkg.Types.projectfile_path(abspath(project))
    context = Pkg.Types.Context(env=Pkg.Types.EnvCache(project_toml_path))

    pkgs = map(Symbol, collect(keys(context.env.project.deps)))
    create_sysimage(pkgs; sysimage_path=joinpath(project, "bin/sys.so"))
end
