#!/bin/bash

check_julia() {
    if ! command -v "$JULIA" >/dev/null 2>&1; then
        echo "julia not found"
        return 1
    fi
}

main() {

export JULIA="${JULIA:-julia}"

echo "Hello, my little friend! You want your Julia to work faster? I'll try to help you..."
echo "More info: https://julialang.github.io/PackageCompiler.jl/dev/devdocs/sysimages_part_1/"

check_julia

TMP_DIR=$(mktemp -d -t JuilaSysImage-XXXXXXXXXX)

if [ ! -d "$TMP_DIR" ]; then
    echo "Failed to create temporary directory"
    return 1
fi

TMP_OBJ=$TMP_DIR/sys.o
OUTPUT=sys.so

BASIC_SYSIMAGE=$(./get_basic_sysimage)

if [ ! -f "$BASIC_SYSIMAGE" ]; then
    echo "Error: BASIC_SYSIMAGE not found"
    return 1
fi

echo "Basic sysimage: $BASIC_SYSIMAGE"

echo "Building '$TMP_OBJ' (it can take a while)..."

2>&1 $JULIA --trace-compile=/dev/stderr --output-o "$TMP_OBJ" custom_sysimage_pkg.jl >/dev/null
# 2>&1 $JULIA --startup-file=no --trace-compile=/dev/stderr --output-o "$TMP_OBJ" -J"$BASIC_SYSIMAGE" custom_sysimage_pkg.jl >/dev/null

if [ ! -f $TMP_OBJ ]; then
    echo "Failed to create object file, returning..."
    return 1
fi

echo "'$TMP_OBJ' is built. Compiling to a shared library..."

LIBDIR=$(./get_libdir)

if [ ! -d "$LIBDIR" ]; then
    echo "LIBDIR not found"
    return 1
fi

CC="${CC:-gcc}"

if ! command -v "$CC" >/dev/null 2>&1; then
    echo "CC not found"
    return 1
fi

$CC -shared -o $OUTPUT -Wl,--whole-archive $TMP_OBJ -Wl,--no-whole-archive -L"$LIBDIR" -ljulia

if [ ! -f "$OUTPUT" ]; then
    echo "Failed to build '$OUTPUT'"
    return 1
fi

echo "Done! Now you can run your program like"
echo "> julia -J/path/to/your/sys.so program.jl"
}

main

echo "Cleaning up"
rm -r $TMP_DIR
