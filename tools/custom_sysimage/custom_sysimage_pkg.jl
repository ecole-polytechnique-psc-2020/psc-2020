Base.reinit_stdio()

Base.init_depot_path()
Base.init_load_path()

import Pkg
Pkg.activate(Base.Filesystem.joinpath(@__FILE__, "../../code/problems/Stocks"))
Pkg.precompile()

empty!(LOAD_PATH)
empty!(DEPOT_PATH)
