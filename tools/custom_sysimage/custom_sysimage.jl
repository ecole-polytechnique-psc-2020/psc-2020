Base.reinit_stdio()

Base.init_depot_path()
Base.init_load_path()

using ReinforcementLearningBase
using ReinforcementLearningCore
using DifferentialEquations
using PyPlot
using Dates
using JuMP
using Cbc
using LinearAlgebra

empty!(LOAD_PATH)
empty!(DEPOT_PATH)
