.PHONY: help header Stocks AlexStocks AlexStocksWithoutSysImage VovaStocks VovaStocksBatch StoreModel AlexStocksV2

help: header
	@echo "This is an entry point to our project. Here is the list of available commands:"
	@echo "Stocks -- anyone, add something here"

header:
	@echo "### Ecole Polytechnique ###"

AlexStocksWithoutSysImage:
	@julia --project=code/problems/Stocks code/problems/Stocks/stocks/exp.jl

VovaStocksBatch: code/problems/Stocks/bin/sys.so stocks_model.bson
	@julia -J code/problems/Stocks/bin/sys.so --project=code/problems/Stocks \
		code/problems/Stocks/stocks/model_evaluator.jl \
		--model stocks_model.bson \
		--days 70 \
		batch --size 100

VovaStocks: code/problems/Stocks/bin/sys.so stocks_model.bson
	@julia -J code/problems/Stocks/bin/sys.so --project=code/problems/Stocks \
		code/problems/Stocks/stocks/model_evaluator.jl \
		--model stocks_model.bson \
		--days 70 \
		oneshot

AlexStocksV2: code/problems/Stocks/bin/sys.so
	@julia -J code/problems/Stocks/bin/sys.so --project=code/problems/Stocks code/problems/Stocks/stocks/exp_v2.jl

AlexStocks: code/problems/Stocks/bin/sys.so
	@julia -J code/problems/Stocks/bin/sys.so --project=code/problems/Stocks code/problems/Stocks/stocks/exp.jl

Stocks: code/problems/Stocks/bin/sys.so
	@julia -J code/problems/Stocks/bin/sys.so --project=code/problems/Stocks code/problems/Stocks/exact_solution.jl

code/problems/Stocks/bin/sys.so: code/problems/Stocks/Project.toml
	@echo "Compiling custom sysimage"
	@mkdir -p code/problems/Stocks/bin
	@julia tools/custom_sysimage/gen_sysimage.jl code/problems/Stocks

stocks_model.bson: code/problems/Stocks/bin/sys.so
	@julia -J code/problems/Stocks/bin/sys.so --project=code/problems/Stocks code/problems/Stocks/stocks/exp.jl

StoreModel:
	@cp stocks_model.bson "models/stocks_model_$$(date -u +%FT%H%M%SZ)".bson
