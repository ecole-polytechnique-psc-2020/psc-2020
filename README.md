# PSC 2020
PSC “Modélisation économique et apprentissage par renforcement”

## Groupe ECO05

- KNIAZEV Leonid
- LANNE Adrien
- OGORODNIKOV Vladimir
- PERMINOV Aleksandr
- SAVIN Joris

## Quick start

TBD

More info:
```
make help
```

## Project structure

```
├── code
│   ├── lib         # Common modules used in all problems
│   └── problems    # Problems that we solve with RL
├── discussions     # Text files with notes, blackboard photos etc.
├── docs
│   ├── lib         # API docs, algorithm description etc.
│   └── problems    # Some papers related to problems
├── experiments     # Code snippets, some files that we want to keep etc.
│   ├── Adrien
│   ├── Aleksandr
│   ├── Joris
│   ├── Leonid
│   └── Vladimir
├── Makefile        # Call interface to the project
├── README.md
└── tools           # Some technical stuff
```
