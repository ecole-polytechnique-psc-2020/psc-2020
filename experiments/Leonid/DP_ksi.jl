using Random
using DifferentialEquations
#using ProfileView
using Distributions

#parameters

Nstep=20

dt=0.1
mu=1.0
sig=0.7
Q95=1.645
Q90=1.282

discreteNstepStockPrice=100
dNSP=discreteNstepStockPrice
maxPriceInit=1.0
maxPriceFactor = exp((mu-sig*sig/2)dt+sig*Q95*sqrt(dt))


discreteNstepCapital=100
dNSC=discreteNstepCapital



NEsp=100

Actions=Array{Int}(0:100)

V=fill(NaN,Nstep+1,dNSC+1,101,dNSP)

A=zeros(Int8,Nstep,dNSC+1,101,dNSP)

nextpriceMeanBase = exp(mu * 2 * dt)
nextpriceVarBase = exp(mu * 4 * dt) * (exp(2 * dt * sig * sig) - 1)
nextpriceVarSqrtBase = sqrt(nextpriceVarBase)

#code

global function nextprice(p::Float64)
    s0 = p/2
    # mean = nextpriceMeanMultiplier * s0
    # var = nextpriceVarMultiplier * s0 * s0
    # println("mean: ", mean)
    # println("variance: ", var)

    return s0 * rand(LogNormal(nextpriceMeanBase, nextpriceVarSqrtBase))
end

# global function nextprice(p::Float64)
#     wiener_process=GeometricBrownianMotionProcess(mu,sig,0.0,p/2)
#     accept_step!(wiener_process,dt,nothing,nothing)
#     accept_step!(wiener_process,dt,nothing,nothing)
#     return wiener_process[3]
# end

function V_calcul(step,NCapital,ksi100,NPrice,maxPrice)
    price=NPrice*maxPrice/dNSP
    maxCapital = 1000 * maxPrice

    Capital=NCapital*maxCapital/dNSC
    if step==Nstep
        V[1+step,1+NCapital,1+ksi100,NPrice]=Capital
    else
        jmin=0
        jmax=100
        T=zeros(101)
        for i = 1:NEsp
            nextPrice=nextprice(price)
            nextNPrice=min(ceil(Int,nextPrice/(maxPrice*maxPriceFactor)*dNSP),dNSP)
            for j = jmin:jmax
		nextCapital = round(Capital * j / price / 100) * nextPrice + Capital * (100 - j)/100
                nextNCapital=min(dNSC,floor(Int,nextCapital*dNSC/maxCapital))
                T[j+1]+=V[1+step+1,1+nextNCapital,1+j,nextNPrice]
            end
        end
        Amax=0
        Vmax=T[1]
        for j = jmin:jmax
            if T[j+1]>Vmax
                Vmax=T[j+1]
                Amax=j
            end
        end
        Vmax/=NEsp
        V[1+step,1+NCapital,1+ksi100,NPrice]=Vmax
        A[1+step,1+NCapital,1+ksi100,NPrice]=Amax
    end
end

function main()
    for s = Nstep:-1 : 0
        println(s)
        maxPrice=maxPriceInit*(maxPriceFactor^s)
        for NC = 0:dNSC
            println(NC)
            for NP = 1:dNSP, ksi100 = 0:100
                V_calcul(s,NC,ksi100,NP,maxPrice)
                #iter -= 1
                #if iter == 0
                #    return
                #end
            end
        end
    end
end

# @profview main(21 * (dNSP + 1) * (dNSD + 1 + 100))
# readline()
#main(21 * (dNSP + 1) * (dNSD + 1 + 100))
main()
