using BSON: @load
using PyPlot

@load "dp_xi.bson" A V

dNSP = 100
maxPriceInit=1.0

dt=0.1
mu=1.0
sig=0.7
Q95=1.645
Q90=1.282

discreteNstepStockPrice=100
dNSP=discreteNstepStockPrice
maxPriceInit=1.0
maxPriceFactor = exp((mu-sig*sig/2)dt+sig*Q95*sqrt(dt))

s = 15

maxPrice=maxPriceInit*(maxPriceFactor^s)

x = zeros(100)
y = zeros(100)

for NPrice in 1:100
	x[NPrice]=NPrice*maxPrice/dNSP

    sumY = 0.0
    cnt = 0

    for i in 1:101
        for j in 1:101
            sumY += A[1 + s, i, j, NPrice]
            cnt += 1
        end
    end

    y[NPrice]=(sumY - 1) / 100.0 / cnt
end

subplot(211)
ylim(-0.05, 1.05)
plot(x,y)
subplot(212)
show()
