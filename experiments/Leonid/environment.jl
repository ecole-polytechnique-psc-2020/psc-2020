__precompile__()

module WienerEnv

using ReinforcementLearningBase
using DifferentialEquations

export WienerEnvironment, get_capital, action

mutable struct WienerEnvironment <: AbstractEnv
    dollars::Float64
    stocks::Int64
    stock_price::Float64
    last_prices::Array{Float64}
    # hint::Float64
    inflation_coefficient::Float64
    old_capital::Float64

    wiener_process::NoiseProcess
end

function WienerEnvironment()
    env = WienerEnvironment(1000, 0, 1, ones(10), 1e-4, 1000, GeometricBrownianMotionProcess(1.0, 0.7, 0.0, 1.0))
    env.wiener_process.dt = 0.1
    setup_next_step!(env.wiener_process, nothing, nothing)
    env
end

RLBase.get_actions(env::WienerEnvironment) = (-1, 0, 1)

get_capital(env::WienerEnvironment) = env.dollars + env.stock_price * env.stocks

RLBase.get_reward(env::WienerEnvironment) = (1 - env.inflation_coefficient) * get_capital(env) - env.old_capital

function RLBase.get_state(env::WienerEnvironment)
    last_prices_count = length(env.last_prices)
    fixed_params_count = 3
    result = Array{Float64, 1}(undef, last_prices_count + fixed_params_count)

    result[1] = env.dollars
    result[2] = env.stocks
    result[3] = env.stock_price
    # result[4] = env.hint
    # result[4] = env.inflation_coefficient
    result[(fixed_params_count+1):(fixed_params_count+last_prices_count)] = env.last_prices

    result
end

RLBase.get_terminal(env::WienerEnvironment) = get_capital(env) <= 0

function RLBase.reset!(env::WienerEnvironment)
    env.dollars = 1000
    env.stocks = 0
    env.stock_price = 1
    env.last_prices = ones(10)
    env.inflation_coefficient = 1e-4
    env.old_capital = 1000

    env.wiener_process = GeometricBrownianMotionProcess(1.0, 0.7, 0.0, 1.0)
    env.wiener_process.dt = 0.1
    setup_next_step!(env.wiener_process, nothing, nothing)
end

function (env::WienerEnvironment)(action)
    if action * env.stock_price <= env.dollars && env.stocks + action >= 0
        env.dollars -= action * env.stock_price
        env.stocks += action
    end

    env.old_capital = get_capital(env)

    accept_step!(env.wiener_process, 0.1, nothing, nothing)

    last_prices_count = length(env.last_prices)
    env.last_prices[1:(last_prices_count-1)] = env.last_prices[2:last_prices_count]
    env.last_prices[last_prices_count] = env.stock_price
    env.stock_price = env.wiener_process[end]
end

end # module
