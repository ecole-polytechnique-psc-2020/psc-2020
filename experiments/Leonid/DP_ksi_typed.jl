# Performance tips taken from here:
# https://www.cs.purdue.edu/homes/hnassar/JPUG/performance.html
using Random
using DifferentialEquations
#using ProfileView
using Distributions
using BSON
using Dates

#parameters

struct ProblemParameters
    NSteps::Int

    dt::Float64
    mu::Float64
    sigma::Float64
    Q95::Float64
    Q90::Float64

    discreteNstepStockPrice::Int
    maxPriceInit::Float64
    maxPriceFactor::Float64

    discreteNstepCapital::Int

    monteCarloIterations::Int
end

struct PriceDistributionParameters
    nextpriceMeanBase::Float64
    nextpriceVarBase::Float64
    nextpriceDistribution::LogNormal
end

function PriceDistributionParameters(params::ProblemParameters)::PriceDistributionParameters
    mu = params.mu
    dt = params.dt
    sigma = params.sigma

    nextpriceMeanBase = exp(mu * 2 * dt)
    nextpriceVarBase = exp(mu * 4 * dt) * (exp(2 * dt * sigma ^ 2) - 1)
    nextpriceVarSqrtBase = sqrt(nextpriceVarBase)
    nextpriceDistribution = LogNormal(nextpriceMeanBase, nextpriceVarSqrtBase)

    PriceDistributionParameters(nextpriceMeanBase, nextpriceVarBase, nextpriceDistribution)
end

struct ProblemOutput
    V::Array{Float64,4}
    A::Array{Int8,4}
end

function ProblemOutput(params::ProblemParameters)::ProblemOutput
    V = fill(NaN, params.NSteps + 1, params.discreteNstepCapital + 1, 101, params.discreteNstepStockPrice)
    A = zeros(Int8, params.NSteps, params.discreteNstepCapital + 1, 101, params.discreteNstepStockPrice)
    ProblemOutput(V, A)
end

# Nstep=20
#
# dt=0.1
# mu=1.0
# sig=0.7
# Q95=1.645
# Q90=1.282
#
# discreteNstepStockPrice=100
# dNSP=discreteNstepStockPrice
# maxPriceInit=1.0
# maxPriceFactor = exp((mu-sig*sig/2)dt+sig*Q95*sqrt(dt))
#
#
# discreteNstepCapital=100
# dNSC=discreteNstepCapital
#
#
#
# NEsp=100
#
# # Actions=Array{Int}(0:100)
#
# V=fill(NaN,Nstep+1,dNSC+1,101,dNSP)
#
# A=zeros(Int8,Nstep,dNSC+1,101,dNSP)
#
# nextpriceMeanBase = exp(mu * 2 * dt)
# nextpriceVarBase = exp(mu * 4 * dt) * (exp(2 * dt * sig * sig) - 1)
# nextpriceVarSqrtBase = sqrt(nextpriceVarBase)
# nextpriceDistribution = LogNormal(nextpriceMeanBase, nextpriceVarSqrtBase)

#code

let
    global nextprice
    rngs::Array{MersenneTwister, 1} = map(_ -> MersenneTwister(Random.make_seed()), 1:Threads.nthreads())

    function nextprice(p::Float64, distr::PriceDistributionParameters)::Float64
        s0 = p/2
        # mean = nextpriceMeanMultiplier * s0
        # var = nextpriceVarMultiplier * s0 * s0
        # println("mean: ", mean)
        # println("variance: ", var)

        return s0 * rand(rngs[Threads.threadid()], distr.nextpriceDistribution)
    end
end

# global function nextprice(p::Float64)
#     wiener_process=GeometricBrownianMotionProcess(mu,sig,0.0,p/2)
#     accept_step!(wiener_process,dt,nothing,nothing)
#     accept_step!(wiener_process,dt,nothing,nothing)
#     return wiener_process[3]
# end

let
    global V_calcul
    T = zeros(101)

    function V_calcul(
            params::ProblemParameters, distr::PriceDistributionParameters,
            output::ProblemOutput, step::Int, NCapital::Int, ksi100::Int,
            NPrice::Int, maxPrice::Float64)

        price=NPrice * maxPrice / params.discreteNstepStockPrice
        maxCapital = 1000 * maxPrice

        Capital=NCapital * maxCapital / params.discreteNstepCapital
        if step==params.NSteps
            output.V[1 + step, 1 + NCapital, 1 + ksi100, NPrice] = Capital
        else
            jmin = 0
            jmax = 100
            fill!(T, 0.0)
            # T = zeros(101)
            for i = 1:(params.monteCarloIterations)
                nextPrice = nextprice(price, distr)
                nextNPrice = min(
                    ceil(Int, nextPrice / (maxPrice * params.maxPriceFactor) * params.discreteNstepStockPrice),
                    params.discreteNstepStockPrice
                )

                for j = jmin:jmax
                    nextCapital = round(Capital * j / price / 100) * nextPrice + Capital * (100 - j) / 100
                    nextNCapital = min(
                        params.discreteNstepCapital,
                        floor(Int, nextCapital * params.discreteNstepCapital / maxCapital)
                    )
                    T[j + 1] += output.V[1 + step + 1, 1 + nextNCapital, 1 + j, nextNPrice]
                end
            end

            Amax = Int8(0)
            Vmax = T[1]
            for j = jmin:jmax
                if T[j+1] > Vmax
                    Vmax = T[j + 1]
                    Amax = Int8(j)
                end
            end

            Vmax /= params.monteCarloIterations
            output.V[1 + step, 1 + NCapital, 1 + ksi100, NPrice] = Vmax
            output.A[1 + step, 1 + NCapital, 1 + ksi100, NPrice] = Amax
        end
        return nothing
    end
end

function calculate(params::ProblemParameters, distr::PriceDistributionParameters)::ProblemOutput
    output = ProblemOutput(params)

    for s = (params.NSteps):-1:0
        println(s)
        maxPrice=params.maxPriceInit*(params.maxPriceFactor^s)
        for NC = 0:params.discreteNstepCapital
            println(NC)
            Threads.@threads for NP = 1:params.discreteNstepStockPrice
                for ksi100 = 0:100
                    V_calcul(params, distr, output, s, NC, ksi100, NP, maxPrice)
                end
            end
        end
    end

    output
end

# @profview main(21 * (dNSP + 1) * (dNSD + 1 + 100))
# readline()
#main(21 * (dNSP + 1) * (dNSD + 1 + 100))
# main()

function main()
    println(string(now()), " Begin caclulations")
    mu = 1.0
    sigma = 0.7
    dt = 0.1
    Q95 = 1.645

    maxPriceFactor = exp((mu - sigma^2 / 2) * dt + sigma * Q95 * sqrt(dt))

    params = ProblemParameters(42, dt, mu, sigma, Q95, 1.282, 100, 1.0, maxPriceFactor, 100, 100)
    distr = PriceDistributionParameters(params)
    output = calculate(params, distr)

    filename = "DP_xi_" * string(now()) * ".bson"
    println(string(now()), " Saving to ", filename)
    bson(filename, A=output.A, V=output.V)
end

main()
