using Dates

println("Debug 1", Dates.now())
include("environment.jl")
println("Debug 3", Dates.now())

using .WienerEnv
println("Debug 2.5", Dates.now())

using ReinforcementLearningBase
using ReinforcementLearningCore
using PyPlot

using StatsBase
using Flux
using ReinforcementLearningZoo

println("Debug 2", Dates.now())

env = WienerEnvironment()
hook = DoEveryNStep(1)  do t, agent, env println(t) end

ns, na = 13, 3
#println(ns, na)

init_Q_agent() = Agent(
    policy=QBasedPolicy(
        learner=TDLearner(
            approximator=TabularApproximator(n_state=ns, n_action=na),
            optimizer=Descent(0.1),
            method=:SARS
            ),
        explorer=EpsilonGreedyExplorer(0.1)
        ),
    trajectory=EpisodicCompactSARTSATrajectory()
)

#run(Agent(;policy=RandomPolicy(env)), env, StopAfterStep(101), hook)
run(init_Q_agent(), env, StopAfterStep(101), hook)
println("Capital ", get_capital(env))
println("Dollars ",env.dollars)
println("Stocks ",env.stocks)
W = env.wiener_process;
plot(1:length(W), W)
show()
