using Random
using Distributions

#parameters

Nstep=20

dt=0.1
mu=1.0
sig=0.7
d=Normal(0.0,sqrt(dt))
Q=quantile.(d,0.95)


discreteNstepStockPrice=1000
dNSP=discreteNstepStockPrice
PriceInit=1.0
maxPriceFactor = exp((mu-sig*sig/2)dt+sig*Q)
minPriceFactor = exp((mu-sig*sig/2)dt-sig*Q)


discreteNstepCapital=1000
dNSC=discreteNstepCapital
CapitalInit=100

NEsp=100

discreteNstepzeta=100
dNSZ=discreteNstepzeta

V=Array{Union{Float32,Missing}}(missing,Nstep+1,dNSC+1,dNSP+1)

A=zeros(Int16,Nstep,dNSC+1,dNSP+1)

#code

global function nextprice(p::Float64)
    p*=exp((mu-sig*sig/2)dt+sig*rand(d))
    return p
end

function V_calcul(step,NCapital,NPrice,maxPrice,maxCapital,minPrice,minCapital)
    price=NPrice*(maxPrice-minPrice)/dNSP+minPrice
    capital=NCapital*(maxCapital-minCapital)/dNSC+minCapital
    nextMaxPrice=maxPrice*maxPriceFactor
    nextMinPrice=minPrice*minPriceFactor
    nextMaxCapital=maxCapital*maxPriceFactor
    nextMinCapital=minCapital*minPriceFactor
    if step==Nstep
        V[1+step,1+NCapital,1+NPrice]=capital
    else
        T=zeros(dNSZ+1)
        for i = 1:NEsp
            nextPrice=nextprice(price)
            nextNPrice=max(min(floor(Int,(nextPrice-nextMinPrice)/(nextMaxPrice-nextMinPrice)*dNSP),dNSP),0)
            for zeta = 0:dNSZ
                nextCapital=capital*zeta/dNSZ/price*nextPrice+capital*(1-zeta/dNSZ)
                nextNCapital=max(0,min(dNSC,floor(Int,(nextCapital-nextMinCapital)/(nextMaxCapital-nextMinCapital)*dNSC)))
                T[zeta+1]+=V[1+step+1,1+nextNCapital,1+nextNPrice]
            end
        end
        Amax=0
        Vmax=T[0+1]
        for zeta = 0:dNSZ
            if T[zeta+1]>Vmax
                Vmax=T[zeta+1]
                Amax=zeta
            end
        end
        Vmax/=NEsp
        V[1+step,1+NCapital,1+NPrice]=Vmax
        A[1+step,1+NCapital,1+NPrice]=Amax
    end
end


for s = Nstep:-1 : 0
    println(s)
    maxPrice=PriceInit*(maxPriceFactor^s)
    maxCapital=CapitalInit/PriceInit*maxPrice
    minPrice=PriceInit*(minPriceFactor^s)
    minCapital=CapitalInit/PriceInit*minPrice
    for NC = 0:dNSC
        println(NC)
        for NP = 0:dNSP
            V_calcul(s,NC,NP,maxPrice,maxCapital,minPrice,minCapital)
        end
    end
end
